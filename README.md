<div>
    <h1>CAROUSEL VERTICAL</h1>
</div>

# Acerca de mí
¡Hola!

Soy Andree, estoy creando mi portafolio en [andreemalerva.com](https://www.andreemalerva.com/), en conjunto a [gitlab](https://gitlab.com/andreemalerva/carousel-vertical-test), actualmente soy Desarrollador Front End, y me agrada seguir aprendiendo.

Trabajemos juntos, te dejo por acá mi contacto.

```
📩 hola@andreemalerva.com
📲 +52 228 353 0727
```

# Acerca del proyecto

Este es un proyecto es un test de carousel en conjunto con HTML, SASS Y JAVASCRIPT

Puedes visualizarlo en la siguiente liga:
[Demo for Andree Malerva](https://andreemalerva.gitlab.io/carousel-vertical-test/) 🫶🏻
